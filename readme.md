# Extraction of tweets for Sentiment Analysis 

The aim is to extract subdatasets from E.Chen OSM geocoded tweets using Elastic search

## Tree of the project
```
.
├── elastic_query: Full elastic query with filter and selected field
│        └── es_query.txt
├── extract_tweets_sentiment_analysis.py: main script
├── htfidf_tfidf_lists_of_results: Directory with H-TFIDF (results are gitignored to not display Twitter's data)
│        ├── h-tfidf-Biggest-score.csv
│        └── TF-IDF_BiggestScore_on_country_corpus.csv
├── LICENSE
├── readme.md
├── readme_ressources: Directory with screenshot or fig. to show

```

## Elastic query
### filters
+ Time period: February 2020
+ Spatial Extent: geocoded user.location "**rest_user_osm.country**": United Kingdom Or France or Italy/Italia
+ Tweet **"lang"**: English

Here is the time serie of Geocoded E.Chen dataset : ![image0001](readme_ressources/image0001.png)

### Select fields:
+ "**id**": Tweet ID
+ "**full_text**": Tweet content
+ "**created_at**": Original tweet date metadata
+ "@**timestamp**": Date in ISO8601 (done during the indexation)
+ "**rest.features.geometry.coordinates**": Geocoded user.location centroid (with OSM data)
+ "**rest_user_osm.country**": country from the geocoding of user.location 

## References
+ [E.Chen dataset repository](https://github.com/echen102/COVID-19-TweetIDs)
+ [E.Chen Related paper](https://arxiv.org/pdf/2003.07372.pdf)
+ [H-TFIDF gitlab repository](https://gitlab.irstea.fr/remy.decoupes/covid19-tweets-mood-tetis)
+ [H-TFIDF Agile'2021 related paper](https://agile-giss.copernicus.org/articles/2/2/2021/)