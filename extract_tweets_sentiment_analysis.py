#!/usr/bin/env python

from elasticsearch import Elasticsearch
from tqdm import tqdm
import csv

def avoid10kquerylimitation(result, client):
    """
    Elasticsearch limit results of query at 10 000. To avoid this limit, we need to paginate results and scroll
    This method append all pages form scroll search
    :param result: a result of a ElasticSearcg query
    :return:
    """
    scroll_size = result['hits']['total']["value"]
    results = []
    # Progress bar
    pbar = tqdm(total=scroll_size)
    while (scroll_size > 0):
        try:
            scroll_id = result['_scroll_id']
            res = client.scroll(scroll_id=scroll_id, scroll='60s')
            results += res['hits']['hits']
            scroll_size = len(res['hits']['hits'])
            pbar.update(scroll_size)
        except:
            pbar.close()
            print("elasticsearch search scroll failed")
            break
    pbar.close()
    return results


if __name__ == '__main__':
    # Elasticsearch parameters
    client = Elasticsearch("http://localhost:9200")
    index = "twitter"
    # script parameters
    query_fname = "elastic_query/es_query.txt"
    output_extracted_fname = "extracted_tweet_output/extracted_geocoded_tweets.csv"

    # Main instructions
    print("#~~~~~ START of extract_tweets_for_sentiment_analysis START ~~~~~#")
    query = open(query_fname, "r").read()
    result = Elasticsearch.search(client, index=index, body=query, scroll='2m', size=5000)
    results = avoid10kquerylimitation(result, client)
    # Save extracted tweets in a csv file
    with open(output_extracted_fname, 'w') as f:  # Just use 'w' mode in 3.x
        header_present = False
        for doc in results:
            row = doc['fields']
            if not header_present:
                w = csv.DictWriter(f, row.keys())
                w.writeheader()
                header_present = True
            # Remove Bracket from value
            for key, value in row.items():
                row[key] = str(value).strip('[]')
            w.writerow(row)
    print("#~~~~~ END of extract_tweets_for_sentiment_analysis END ~~~~~#")